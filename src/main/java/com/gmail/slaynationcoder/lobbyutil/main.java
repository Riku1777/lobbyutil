package com.gmail.slaynationcoder.lobbyutil;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Slaynation_Coder on 2016/05/03.
 */
public class main extends JavaPlugin implements Listener
{
    List<Player> PVPer = new ArrayList<>();

    public static void removeItemstack(Player me, Material material)
    {
        for (ItemStack invItem : me.getInventory().getContents())
        {
            if (invItem == null)
                continue;

            if (invItem.getType() == material)
                me.getInventory().remove(invItem);
        }
    }

    public void onEnable()
    {
        getServer().getPluginManager().registerEvents(this, this);
    }

    public void onDisable()
    {

    }

    @EventHandler
    public void onLaunchPad(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();

        if (PVPer.contains(player))
            return;

        if (event.getAction() != Action.PHYSICAL)
            return;

        Block clickedBlock = event.getClickedBlock();

        if (clickedBlock.getType() != Material.GOLD_PLATE && clickedBlock.getType() != Material.IRON_PLATE)
            return;

        if (clickedBlock.getRelative(BlockFace.DOWN) == null || clickedBlock.getRelative(BlockFace.DOWN).getType() != Material.REDSTONE_BLOCK)
            return;

        //jump
        Vector vector = event.getPlayer().getLocation().getDirection().multiply(4);

        new BukkitRunnable()
        {
            public void run()
            {
                player.setVelocity(new Vector(vector.getX(), 1.2D, vector.getZ()));
            }
        }.runTaskLater(this, 1L);

        new BukkitRunnable()
        {
            public void run()
            {
                player.setVelocity(new Vector(vector.getX(), 1.2D, vector.getZ()));
            }
        }.runTaskLater(this, 2L);
    }

    @EventHandler
    public void onJoinPlayer(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();

        player.teleport(player.getWorld().getSpawnLocation().clone().add(0.5, 0, 0.5));

        giveDefault(player);
        DisablePVP(player);
    }

    @EventHandler
    public void onLeftPlayer(PlayerQuitEvent event)
    {
        Player player = event.getPlayer();
        if (PVPer.contains(player))
            DisablePVP(player);
    }

    @EventHandler
    public void onTogglePvP(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();

        if (PVPer.contains(player))
            return;

        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;

        ItemStack iteminHand = player.getItemInHand();
        if (iteminHand == null || iteminHand.getItemMeta() == null || iteminHand.getItemMeta().getDisplayName() == null)
            return;

        if (!(ChatColor.stripColor(iteminHand.getItemMeta().getDisplayName())).equals("PVP Mode - OFF"))
            return;

        event.setCancelled(true);
        EnablePVP(player);
    }

    public void giveDefault(Player player)
    {
        //give Toggle PVP Tool
        ItemStack togglePvp = new ItemStack(Material.IRON_SWORD);
        ItemMeta itemMeta = togglePvp.getItemMeta();
        itemMeta.setDisplayName(ChatColor.RESET + "" + ChatColor.GREEN + "PVP Mode" + ChatColor.RESET + " - " + ChatColor.RED + "OFF");
        togglePvp.setItemMeta(itemMeta);

        player.getEquipment().clear();
        player.getInventory().clear();

        player.getInventory().addItem(togglePvp);
    }

    public void EnablePVP(Player player)
    {
        PVPer.add(player);

        for (Player p : getServer().getOnlinePlayers())
        {
            if (!PVPer.contains(p))
            {
                p.hidePlayer(player);
                player.hidePlayer(p);
            }
            else
            {
                p.showPlayer(player);
                player.showPlayer(p);
            }
        }

        player.getEquipment().clear();
        player.getInventory().clear();

        ItemStack diamondSword = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta itemMeta = diamondSword.getItemMeta();
        itemMeta.setDisplayName(ChatColor.RESET + "" + ChatColor.GREEN + "PVP Mode" + ChatColor.RESET + " - " + ChatColor.YELLOW + "ON");
        itemMeta.addEnchant(Enchantment.KNOCKBACK, 1, true);
        diamondSword.setItemMeta(itemMeta);

        player.setItemInHand(diamondSword);
        player.getInventory().addItem(new ItemStack(Material.BOW));
        player.getInventory().addItem(new ItemStack(Material.ARROW, 64));
        player.getInventory().addItem(new ItemStack(Material.APPLE, 64));

        player.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
        player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
        player.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
        player.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));

        player.playSound(player.getLocation(), Sound.CLICK, 1, 10);

    }

    public void DisablePVP(Player player)
    {
        PVPer.remove(player);

        for (Player p : getServer().getOnlinePlayers())
        {
            if (PVPer.contains(p))
            {
                p.hidePlayer(player);
                player.hidePlayer(p);
            }
            else
            {
                p.showPlayer(player);
                player.showPlayer(p);
            }
        }
    }

    @EventHandler
    public void onDeathPlayer(PlayerDeathEvent event)
    {
        Player player = event.getEntity();
        if (PVPer.contains(player))
        {
            event.getDrops().clear();
            event.getDrops().add(new ItemStack(Material.GOLDEN_APPLE));
            DisablePVP(player);
        }
    }

    @EventHandler
    public void pickupGoldenApple(PlayerPickupItemEvent event)
    {
        ItemStack pickup = event.getItem().getItemStack();
        if (pickup.getType() != Material.GOLDEN_APPLE || !PVPer.contains(event.getPlayer()))
        {
            //NO PVPer or NO GOLDEN APPLE
            event.setCancelled(true);
            return;
        }

        Player player = event.getPlayer();
        //PVP Remove Apple
        new BukkitRunnable()
        {
            public void run()
            {
                removeItemstack(player, Material.GOLDEN_APPLE);
            }
        }.runTaskLater(this, 1);

        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 120, 3));
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event)
    {
        giveDefault(event.getPlayer());
    }

    @EventHandler
    public void onAntiVoid(PlayerMoveEvent event)
    {
        if (PVPer.contains(event.getPlayer()))
            return;

        if (event.getTo().getY() <= 0)
        {
            event.getPlayer().setFallDistance(0);
            event.getPlayer().teleport(event.getPlayer().getWorld().getSpawnLocation().clone().add(0.5, 0, 0.5));
        }
    }

    @EventHandler
    public void onDamagePlayer(EntityDamageEvent event)
    {
        if (event.getEntity().getType() == EntityType.PLAYER)
        {
            if (PVPer.contains((Player) event.getEntity()))
                return;

            event.setDamage(0);
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event)
    {
        if (PVPer.contains((Player) event.getEntity()))
            return;

        event.setFoodLevel(20);
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent event)
    {
        event.setCancelled(true);
    }
}
